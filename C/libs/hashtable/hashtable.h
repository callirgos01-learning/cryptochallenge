
#ifndef __HASHTABLE_H__ 
#define __HASHTABLE_H__

void HashTable_Init( );
void HashTable_InsertValue( char *valueToAdd );
void HashTable_RemoveValue( char *valueToRemove );
char HashTable_IsPresent( char *toLookUp );

#endif // __HASHTABLE_H__
