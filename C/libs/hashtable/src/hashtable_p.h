

#ifndef __HASHTABLE_P_H__ 
#define __HASHTABLE_P_H__
#include "../../linkedlist/linkedlist.h"

#define HASHTABLE_LINKEDLIST_LENGTH (5)
typedef struct
{
    LinkedList_Self linkedSelf;
    LinkedList_Node linkedList[HASHTABLE_LINKEDLIST_LENGTH];
}HashTable_Table;

#endif // __HASHTABLE_P_H__