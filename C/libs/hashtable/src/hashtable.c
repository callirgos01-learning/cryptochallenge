#include <stdio.h>
#include <string.h>
#include "hashtable_p.h"
#include "../hashtable.h"
#include "../../linkedlist/linkedlist.h"

#define HASH_LENGTH (256)


//static LinkedList_Node s_allTheNodes[HASH_LENGTH][HASHTABLE_LINKEDLIST_LENGTH] = {0};
static HashTable_Table s_allTheNodes[HASH_LENGTH] = { 0 };

static unsigned char hash(const char *input, unsigned int inputSize)
{
    unsigned char toReturn = 0;
    while ( *(input) != 0 )
    {
        //toReturn ^= *(input++);
        toReturn = (toReturn<<5) - toReturn + *(input++);
    } 
    return toReturn;

}

void HashTable_Init( )
{
    int i;
    for(i=0;i<HASH_LENGTH;i++)
    {
        printf("hash index %u\r\n", i);
        LinkedList_Init( &s_allTheNodes[i].linkedSelf, s_allTheNodes[i].linkedList, HASHTABLE_LINKEDLIST_LENGTH );
    }
}

unsigned int HashTable_GetMaxNumberOfHashEntries()
{
    return HASH_LENGTH;
}

unsigned int HashTable_GetMaxNumberOfCollisionsSupported()
{
    return HASHTABLE_LINKEDLIST_LENGTH;
}

void HashTable_InsertValue( char *valueToAdd )
{
    unsigned int index = hash(valueToAdd, strlen(valueToAdd));
    LinkedList_Add( &s_allTheNodes[index].linkedSelf, valueToAdd );
    printf("inserting[%i] %s \r\n", index, valueToAdd );
}

void HashTable_RemoveValue( char *valueToRemove )
{
    unsigned int index = hash(valueToRemove, strlen(valueToRemove));
    LinkedList_Remove( &s_allTheNodes[index].linkedSelf, valueToRemove );
    printf("removing[%i] %s \r\n", index, valueToRemove );
}

char HashTable_IsPresent( char *toLookUp )
{    
    unsigned int index = hash(toLookUp, strlen(toLookUp));    
    return LinkedList_Find( &s_allTheNodes[index].linkedSelf, toLookUp );
}