
#ifndef __LINKEDLIST_H__ 
#define __LINKEDLIST_H__

typedef struct
{
    char *value;
    void *next;
}LinkedList_Node;

typedef struct
{    
    char initialized;
    LinkedList_Node *freeList;
    LinkedList_Node *usedList;
    char nodesHeapSize;
    char nodeCount;
}LinkedList_Self;


void LinkedList_Init( LinkedList_Self *self, LinkedList_Node *linkedListNodes, unsigned int size );
void LinkedList_Add( LinkedList_Self *self, char *valueToAdd );
void LinkedList_Remove( LinkedList_Self *self, char *valueToRemove );
char LinkedList_Find( LinkedList_Self *self, char *valueToFind );

#endif // __LINKEDLIST_H__
