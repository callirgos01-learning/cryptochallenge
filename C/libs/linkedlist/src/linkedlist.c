#include <stdio.h>
#include "../linkedlist.h"
#include "linkedlist_p.h"

static void LinkedList_PrintLinkedList( LinkedList_Self *self, char *listName, LinkedList_Node *listToPrint )
{
	if( listToPrint == NULL )
	{
		printf("%s list is empty\r\n", listName);
	}
	else
	{
		LinkedList_Node *head = listToPrint;
		printf("==============================\r\n");
		while(head != NULL)
		{
			printf("%s(0x%08X){ .value(0x%08X) = %s; .next = 0x%08X; }\r\n", listName, head, head->value, head->value, head->next);		
			head = head->next;
		}		
		printf("==============================\r\n");
	}	
}
static void LinkedList_PrintAllNodes( LinkedList_Self *self, LinkedList_Node *linkedListNodes, unsigned int size )
{
	int i;
    printf("**********************************\r\n");
	for( i = 0; i < size; i++ )
	{
		printf("linkedList[%u] = { .value = 0x%08X; .next = 0x%08X; }\r\n", i, linkedListNodes[i].value, linkedListNodes[i].next);
	}
    printf("**********************************\r\n");
}
static void LinkedList_FreeNode(LinkedList_Self *self, LinkedList_Node *toAdd)
{
    //save the address of the usedList
    LinkedList_Node *freeHead = self->freeList;
    if(freeHead == NULL)
    {
        self->freeList = toAdd;
        self->freeList->value = NULL;
        self->freeList->next = NULL;
    }
    else
    {
        //add the node on the top of the list
        self->freeList = toAdd;
        self->freeList->value = NULL;
        //link the new head to the next element in the usedList
        self->freeList->next = freeHead;    
    }
    
}

static LinkedList_Node *LinkedList_GetAvailableNode(LinkedList_Self *self, char *valueToAdd, LinkedList_Node *nextPointer)
{
	LinkedList_Node *toReturn = NULL;
	//handle case where we are out of nodes!!
	if(self->freeList != NULL)
	{
		//save the address of the next free element in the list
		LinkedList_Node *temp = self->freeList->next;
		//return the first element in the free list
		toReturn = self->freeList;
		//toReturn set value to incoming value
		toReturn->value = valueToAdd;
		//link to the nextinpute pointer
		toReturn->next = nextPointer;
		//save the address of the next element in the free list as the head of the free list
		self->freeList = temp;
		//return the address to the first element in the free list to be added the used list
	}
	else
	{
		printf("OUT OF NODES!!\r\n");
		//we could implement a circular linked list here, and pop out the last element in the list for this one.

	}
	return toReturn;
}
void LinkedList_Init( LinkedList_Self *self, LinkedList_Node *linkedListNodes, unsigned int size )
{
	int i;
	//LinkedList_PrintAllNodes( linkedListNodes, size );

	for( i = 0; i < size - 1; i++ )
	{
		linkedListNodes[i].value = 0;
		linkedListNodes[i].next = &linkedListNodes[i+1];
	}
	linkedListNodes[size-1].value = 0;
	linkedListNodes[size-1].next = NULL;

	//LinkedList_PrintAllNodes( linkedListNodes, size );

	self->freeList = linkedListNodes;
	self->usedList = NULL;
	self->nodesHeapSize = size; 
	self->nodeCount = 0;

	//LinkedList_PrintLinkedList( self, "freeList", self->freeList);
	//LinkedList_PrintLinkedList( self, "usedList", self->usedList);
	self->initialized = 1;
}

static void LinkedList_AddFirst( LinkedList_Self *self, char *valueToAdd )
{
	if( !self->initialized )
	{
		printf("UNABLE TO USE AN INITIALIZED LINKED LIST USE LinkedList_Init FIRST\r\n ");		
	}
	else
	{
		LinkedList_Node *newNode = LinkedList_GetAvailableNode(self, valueToAdd, NULL);
		if( newNode != NULL )
		{
			//save the address of the usedList
			LinkedList_Node *usedHead = self->usedList;
			//put the new node on the top of the used list.		
			self->usedList = newNode;
			//link the new head to the next element in the usedList
			self->usedList->next = usedHead;
		}
		else
		{
			printf("operation failed, cant get more nodes\r\n");
		}
		// LinkedList_PrintLinkedList( self, "freeList", self->freeList );
		//LinkedList_PrintLinkedList( self, "usedList", self->usedList );
	}
}

static void LinkedList_AddLast( LinkedList_Self *self, char *valueToAdd )
{
	if( !self->initialized )
	{
		printf("UNABLE TO USE AN INITIALIZED LINKED LIST USE LinkedList_Init FIRST\r\n ");		
	}
	else
	{
		if( self->usedList == NULL )
		{
			LinkedList_AddFirst(self, valueToAdd);
		}
		else
		{
			LinkedList_Node *newNode = LinkedList_GetAvailableNode(self, valueToAdd, NULL);
			if( newNode != NULL )
			{
				//get to the end of the used list
				LinkedList_Node *usedListIterator = self->usedList;
				//iterate through the linked list until you find the end of the list
				while(usedListIterator->next != NULL)
				{
					usedListIterator = usedListIterator->next;
				}
				usedListIterator->next = newNode;
			}
			else
			{
				printf("operation failed, cant get more nodes\r\n");
			}
			
		}
		//LinkedList_PrintLinkedList( self, "freeList", self->freeList );
		//LinkedList_PrintLinkedList( self, "usedList", self->usedList );
	}
}

void LinkedList_Add( LinkedList_Self *self, char *valueToAdd )
{
	LinkedList_AddLast( self, valueToAdd );
}

char LinkedList_Find( LinkedList_Self *self, char *valueToFind )
{
	char toReturn = 0;

	LinkedList_Node *head = self->usedList;

	while(head != NULL)
	{
		if(strcmp(head->value, valueToFind) == 0)
		{
			toReturn = 1;
			break;
		}
		head = head->next;
	}

	return toReturn;

}

void LinkedList_Remove( LinkedList_Self *self, char *valueToRemove )
{
	if( !self->initialized )
	{
		printf("UNABLE TO USE AN INITIALIZED LINKED LIST USE LinkedList_Init FIRST\r\n ");		
	}
	else
	{
		if( self->usedList == NULL )
		{
			printf("list is empty nothing to remove\r\n ");	
		}
		else
		{			
			if( strcmp( self->usedList->value, valueToRemove ) == 0 )
			{
               // printf("REMOVING HEAD \r\n ");
                LinkedList_Node *toFree = self->usedList;                
                //remove node
				self->usedList = self->usedList->next;
                LinkedList_FreeNode( self, toFree );
            }   
			else
			{
				LinkedList_Node *cur = self->usedList;
				LinkedList_Node *prev = NULL; 
				while ( (cur != NULL) && (strcmp(cur->value, valueToRemove) != 0) )
				{
                    prev = cur;
                    cur = cur->next;
				}
                if(cur == NULL)
                {
                    printf("UNABLE TO FIND KEY VALUE [%s]\r\n", valueToRemove);
                }
                else//delete current node
                {
                   // printf("removing [%s]\r\n", valueToRemove);
                    prev->next = cur->next;
                    LinkedList_FreeNode( self, cur ); 
                }
			}
		}
		//LinkedList_PrintLinkedList( self, "freeList", self->freeList);
		//LinkedList_PrintLinkedList( self, "usedList", self->usedList);
	}

}