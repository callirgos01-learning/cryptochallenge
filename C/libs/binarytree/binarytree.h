
#ifndef __BINARYTREE_H__ 
#define __BINARYTREE_H__
#include "../queue/queue.h"

#define QUEUE_SIZE ( 500 )

typedef struct 
{
    unsigned int key;
    void *parent;
    void *left;
    void *right;
    unsigned int redBlack;
    unsigned int height;
}BinaryTree_Node;

typedef struct
{    
    char initialized;
    unsigned int maxNumberOfNodes;
    BinaryTree_Node *freeTree;
    BinaryTree_Node *binaryTree;
    Queue_Self queueSelf;
    Queue_Node queueNodes[QUEUE_SIZE];
}BinaryTree_Self;

void BinaryTree_Init( BinaryTree_Self *self, BinaryTree_Node *binaryTreeNodes, unsigned int size );
void BinaryTree_Insert( BinaryTree_Self *self, unsigned int key );
char BinaryTree_Search( BinaryTree_Self *self, unsigned int key );
unsigned int BinaryTree_Min( BinaryTree_Self *self);
unsigned int BinaryTree_Max( BinaryTree_Self *self);
unsigned int BinaryTree_Successor( BinaryTree_Self *self, unsigned int key ); 
unsigned int BinaryTree_Predecesor( BinaryTree_Self *self, unsigned int key );
void BinaryTree_PrintTreeInOrder( BinaryTree_Self *self );
void BinaryTree_Remove( BinaryTree_Self *self, unsigned int key );
void BinaryTree_PrintTreeLevelOrder( BinaryTree_Self *self );

#endif // __BINARYTREE_H__
