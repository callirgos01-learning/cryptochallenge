#include <stdio.h>
#include "../binarytree.h"
#include "binarytree_p.h"
#include "../../queue/queue.h"

static int BinaryTree_GetKey( BinaryTree_Self *self, BinaryTree_Node *node )
{
	int toReturn = -1;
	if( node != NULL )
	{
		toReturn = node->key;
	}
	return toReturn;
}

static void BinaryTree_PrintTreeLevels( BinaryTree_Self *self, char* name, BinaryTree_Node *root )
{
	//breadth first traversal on a tree
	if( self->initialized == 1 )
	{
		if( root != NULL )
		{
			if( root->left != NULL )
			{
				Queue_EnQueue( &self->queueSelf, root->left );
			}
			if( root->right != NULL )
			{
				Queue_EnQueue( &self->queueSelf, root->right );
			}
			printf("%s { .key = %03u; .parent = %03i; .left = %03i; .right = %03i; .height = %03i}\r\n",	
										name,
										BinaryTree_GetKey(self, root), 
										BinaryTree_GetKey(self, root->parent), 
										BinaryTree_GetKey(self, root->left), 
										BinaryTree_GetKey(self, root->right),
										root->height );			
			BinaryTree_PrintTreeLevels( self, name, Queue_DeQueue(&self->queueSelf) );
		}
	}
}

void BinaryTree_PrintTreeLevelOrder( BinaryTree_Self *self )
{
	BinaryTree_PrintTreeLevels( self, "levelTree", self->binaryTree );
}
static unsigned int BinaryTree_GetHeight( BinaryTree_Self *self, BinaryTree_Node *root )
{
	unsigned int toReturn = 0;
	if( root != NULL )
	{
		toReturn = root->height;
		//printf("BinaryTree_GetHeight %u is %u\r\n", root->key, toReturn);
	}

	return toReturn;

}
static unsigned int BinaryTree_CalculateHeight( BinaryTree_Self *self, BinaryTree_Node *root )
{
 	unsigned int toReturn = 0;
	if( root != NULL )
	{
		unsigned int heightLeft = 0;
		unsigned int heightRight = 0;
		if( root->left != NULL )
		{
			heightLeft = BinaryTree_GetHeight( self, root->left );
		}
		if( root->right != NULL )
		{
			heightRight = BinaryTree_GetHeight( self, root->right );
		}

		root->height = 1 + (( heightLeft > heightRight ) ? heightLeft : heightRight );

		toReturn = root->height;
	}
	//printf("BinaryTree_CalculateHeight  %u is %u \r\n", root->key, toReturn);
	return toReturn;
}


static void BinaryTree_ReCalculateHeight( BinaryTree_Self *self, BinaryTree_Node *root )
{
	if( root != NULL )
	{
		BinaryTree_CalculateHeight( self, root );
		BinaryTree_ReCalculateHeight( self, root->parent );
	}
}
static void BinaryTree_PrintAllNodes( BinaryTree_Self *self, BinaryTree_Node *binaryTreeNodes, unsigned int size )
{
	int i;
    printf("**********************************\r\n");
	for( i = 0; i < size; i++ )
	{
		printf("binarytree[%02u](0x%08X) = { .key = %u; .parent = 0x%08X; .left = 0x%08X; .right = 0x%08X; }\r\n", i, &binaryTreeNodes[i], binaryTreeNodes[i].key, binaryTreeNodes[i].parent, binaryTreeNodes[i].left, binaryTreeNodes[i].right );
	}
    printf("**********************************\r\n");
}

static void BinaryTree_PrintTree( BinaryTree_Self *self, char* name, BinaryTree_Node *root )
{
	if( self->initialized == 1 )
	{    	
		if(root != NULL)
		{
			BinaryTree_PrintTree(self, name, root->left);
			printf("%s { .key = %03u; .parent = %03i; .left = %03i; .right = %03i; .height = %03i}\r\n",	
										name,
										BinaryTree_GetKey(self, root), 
										BinaryTree_GetKey(self, root->parent), 
										BinaryTree_GetKey(self, root->left), 
										BinaryTree_GetKey(self, root->right),
										root->height );
			BinaryTree_PrintTree(self, name, root->right);
		}
    	
	}
}

void BinaryTree_PrintTreeInOrder( BinaryTree_Self *self )
{
	BinaryTree_PrintTree(self, "inOrder", self->binaryTree);	
}
void BinaryTree_Init( BinaryTree_Self *self, BinaryTree_Node *binaryTreeNodes, unsigned int size )
{
	int i;
	binaryTreeNodes[0].parent = NULL;
	binaryTreeNodes[0].left = NULL;
	binaryTreeNodes[0].right = binaryTreeNodes + 1;
	binaryTreeNodes[0].height = 0;
	for( i = 1; i < size - 1; i++ )
	{
		binaryTreeNodes[i].key = 0;
		binaryTreeNodes[i].parent = &binaryTreeNodes[i - 1];
		binaryTreeNodes[i].left = NULL;
		binaryTreeNodes[i].right = &binaryTreeNodes[i + 1];		
		binaryTreeNodes[i].height = 0;
	}
	binaryTreeNodes[size - 1].key = size - 1;
	binaryTreeNodes[size - 1].parent = &binaryTreeNodes[size - 2];
	binaryTreeNodes[size - 1].left = NULL;
	binaryTreeNodes[size - 1].right = NULL;
	binaryTreeNodes[i].height = 0;

	self->freeTree = binaryTreeNodes;
	self->binaryTree = NULL;	
	self->initialized = 1;
	Queue_Init( &self->queueSelf, self->queueNodes, QUEUE_SIZE );
}

static BinaryTree_Node *BinaryTree_GetNextAvailable( BinaryTree_Self *self, unsigned int key, BinaryTree_Node *root  )
{
	BinaryTree_Node *toReturn = NULL;
	if( self->freeTree == NULL )
	{
		///@todo: add some way to assert, or propagate fatal errors to the client.
		printf("NO MORE NODES TO GET, WE HAVE REACHED THE LIMIT OF OUR CLIENT ALLOCATED MEMORY\r\n");
	}
	else
	{
		toReturn = self->freeTree;
		self->freeTree = self->freeTree->right;
		if( self->freeTree != NULL )
		{
			self->freeTree->parent = NULL;
		}

		toReturn->key = key;
		toReturn->parent = root;
		toReturn->height = 1;
		toReturn->left = NULL;
		toReturn->right = NULL;
	}
	return toReturn;
}

static void BinaryTree_FreeNode( BinaryTree_Self *self, BinaryTree_Node *nodeToFree )
{	
	if( nodeToFree != NULL )
	{
		nodeToFree->key = 0;		
		nodeToFree->parent = NULL;
		nodeToFree->right = NULL;
		nodeToFree->left = NULL;

		if( self->freeTree == NULL )
		{
			//free tree is empty
			//set this node as the root
			self->freeTree = nodeToFree;
		}
		else
		{
			//free tree is not empty
			//pop it in as root
			//save old root address
			BinaryTree_Node *oldRoot = self->freeTree;
			//make the note to free the root of this tree
			self->freeTree = nodeToFree;
			//link the old root to the right of the new root
			self->freeTree->right = oldRoot;
			//make the old root parent, the new root.
			oldRoot->parent = self->freeTree;

		}
	}
}
static BinaryTree_Node *BinaryTree_SearchHelper(  BinaryTree_Self *self, BinaryTree_Node *root, unsigned int key )
{
	BinaryTree_Node *toReturn = NULL;
	//printf("[%u]", key);
	if( self->initialized == 1 )
	{
		//traverse the tree to find the place to put this key
		if( root != NULL )
		{				
			if( root->key == key )
			{
				//found the key you were looking for
				//return the node it contains it, with its children
				return root;

			}
			else if( root->key > key )
			{
				if(root->left == NULL)
				{
					return root;
				}
				else
				{
					//continue left
					return BinaryTree_SearchHelper( self, root->left, key );
				}
			}
			else
			{
				if(root->right == NULL)
				{
					return root;
				}
				else
				{
					//continute right
					return BinaryTree_SearchHelper( self, root->right, key );
					
				}
			}
		}
		//printf("**********************************\r\n");
		//BinaryTree_PrintTree(self, "binaryTree",self->binaryTree);
		//printf("**********************************\r\n");
	}
	else
	{
		printf("PLEASE USED BinaryTree_Init BEFORE INSERTING NODES\r\n");
	}
	return toReturn;
}

char BinaryTree_Search( BinaryTree_Self *self, unsigned int key )
{
	char toReturn = 0;		
	BinaryTree_Node *root = BinaryTree_SearchHelper(self, self->binaryTree, key );

	if( root != NULL )
	{
		toReturn = ( root->key == key );
	}

	return toReturn;
}

static BinaryTree_Node *BinaryTree_MinHelper( BinaryTree_Self *self, BinaryTree_Node *root  )
{
	BinaryTree_Node *toReturn = NULL;
	if( self->initialized == 1 )
	{
		if( root != NULL )
		{
			if( root->left != NULL )
			{
				toReturn = BinaryTree_MinHelper( self, root->left );
			}
			else
			{	
				toReturn = root;
			}		
		}
	}
	else
	{
		printf("PLEASE USED BinaryTree_Init BEFORE TRAVERSING THROUGH FOR NODES\r\n");
	}
	return toReturn;

}

unsigned int BinaryTree_Min( BinaryTree_Self *self )
{
	unsigned int toReturn = 0;
	BinaryTree_Node *min = BinaryTree_MinHelper( self, self->binaryTree );
	if( min != NULL )
	{
		toReturn = min->key;
	}
	return toReturn;
}
static BinaryTree_Node *BinaryTree_MaxHelper( BinaryTree_Self *self, BinaryTree_Node *root )
{

	BinaryTree_Node *toReturn = 0;

	if( self->initialized == 1 )
	{
		if(root != NULL)
		{
			//printf("[%u]\r\n", root->key);			
			if( root->right != NULL )
			{
				toReturn = BinaryTree_MaxHelper( self, root->right );
			}
			else
			{	
				toReturn = root;
			}			
		}
	}
	else
	{
		printf("PLEASE USED BinaryTree_Init BEFORE TRAVERSING THROUGH FOR NODES\r\n");
	}

	return toReturn;
}
unsigned int BinaryTree_Max( BinaryTree_Self *self )
{

	unsigned int toReturn = 0;
	BinaryTree_Node *max = BinaryTree_MaxHelper( self, self->binaryTree );
	if( max != NULL )
	{
		toReturn = max->key;
	}
	return toReturn;
}

static BinaryTree_Node *BinaryTree_SuccessorHelper(  BinaryTree_Self *self, BinaryTree_Node *root, unsigned int key )
{
	BinaryTree_Node *toReturn = NULL;
	if( self->initialized == 1 )
	{
		if( root != NULL )
		{
			BinaryTree_Node *keyNode = BinaryTree_SearchHelper( self, root, key );
			if( keyNode->key == key )
			{
				if( keyNode->right != NULL )
				{
					toReturn = BinaryTree_MinHelper( self, keyNode->right );					
				}
				else
				{
					//search parents until you find the first key that is bigger than key
					keyNode = keyNode->parent;
					while(keyNode != NULL)
					{
						//printf("[%u]\r\n", keyNode->key);
						if( keyNode->key > key )
						{
							//found the first parent with a bigger value than key
							break;
						}
						keyNode = keyNode->parent;
					}
					toReturn = keyNode;
					
				}
			}
			else
			{
				printf("unable to find key %u in tree \r\n", key);	
			}
		}
	}
	else
	{
		printf("PLEASE USED BinaryTree_Init BEFORE TRAVERSING THROUGH FOR NODES\r\n");
	}
	return toReturn;
}
unsigned int BinaryTree_Successor( BinaryTree_Self *self, unsigned int key )
{
	unsigned int toReturn = 0;
	BinaryTree_Node *successor = BinaryTree_SuccessorHelper( self, self->binaryTree, key );
	if( successor != NULL )
	{
		toReturn = successor->key;
	}
	return toReturn;
}
static BinaryTree_Node *BinaryTree_PredecesorHelper( BinaryTree_Self *self, BinaryTree_Node *root, unsigned int key )
{
	BinaryTree_Node *toReturn = NULL;
	if( self->initialized == 1 )
	{
		if( root != NULL )
		{
			BinaryTree_Node *keyNode = BinaryTree_SearchHelper( self, root, key );
			if( keyNode->key == key )
			{
				if( keyNode->left != NULL )
				{
					toReturn = BinaryTree_MaxHelper(self, keyNode->left);					
				}
				else
				{					
					keyNode = keyNode->parent;
					while(keyNode != NULL)
					{						
						if( keyNode->key < key )
						{
							break;
						}
						keyNode = keyNode->parent;
					}
					toReturn = keyNode;						
				}
			}
			else
			{
				printf("unable to find key %u in tree \r\n", key);
			}

		}
	}
	return toReturn;
}


unsigned int BinaryTree_Predecesor( BinaryTree_Self *self, unsigned int key )
{	
	unsigned int toReturn = 0;
	BinaryTree_Node *pred = BinaryTree_PredecesorHelper( self, self->binaryTree, key );
	if( pred != NULL )
	{
		toReturn = pred->key;
	}
	return toReturn;
}
static void BinaryTree_Swap( BinaryTree_Self *self, BinaryTree_Node *node1, BinaryTree_Node *node2 )
{
	if( (node1 != NULL) && (node2 != NULL) )
	{
		/*printf("**********************************\r\n");
		BinaryTree_PrintTree(self, "node1",node1);
		printf("**********************************\r\n");
		printf("**********************************\r\n");
		BinaryTree_PrintTree(self, "node2",node2);
		printf("**********************************\r\n");
*/
		BinaryTree_Node *temp = node1;
		node1 = node2;
		node2 = temp;

		/*node1->key = node2->key;
		node1->parent = node2->parent;
		node1->left = node2->left;
		node1->right = node2->right;

		node2->key = temp.key;
		node2->parent = temp.parent;
		node2->left = temp.left;
		node2->right = temp.right;*/
/*
		printf("**********************************\r\n");
		BinaryTree_PrintTree(self, "node1",node1);
		printf("**********************************\r\n");
		printf("**********************************\r\n");
		BinaryTree_PrintTree(self, "node2",node2);
		printf("**********************************\r\n");
*/
	}

}

static BinaryTree_RotateRight( BinaryTree_Self *self, BinaryTree_Node *root )
{
	if( root != NULL )
	{
		/*
		struct node *x = y->left;
		struct node *T2 = x->right;

		// Perform rotation
		x->right = y;
		y->left = T2;

		*/

		printf("BinaryTree_RotateRight %u\r\n", root->key);
		BinaryTree_PrintTree(self, "before", root);


		BinaryTree_Node *newRoot = root->left;
		BinaryTree_Node *oldRoot = root;
	    BinaryTree_Node *newRootOldRight = newRoot->right;
	 	BinaryTree_Node *oldRootParent = root->parent;
		printf("BinaryTree_RotateRight newRoot %i\r\n", BinaryTree_GetKey(self, newRoot));
		printf("BinaryTree_RotateRight oldRoot %i\r\n", BinaryTree_GetKey(self, oldRoot));
		printf("  newRootOldRight %i\r\n", BinaryTree_GetKey(self, newRootOldRight));
		printf("BinaryTree_RotateRight oldRootParent %i\r\n", BinaryTree_GetKey(self, oldRootParent));

		root = root->left;

	    // Perform rotation
	    //replace root left right with root
	    newRoot->right = oldRoot;
	    //update root's parent
	    oldRoot->parent = newRoot;

	    //repalce root left with whatever there was in the right side of the root left node	    
	    oldRoot->left = newRootOldRight;
	    //if whatever was there was not null, then update the parent of said node.
	    if( newRootOldRight != NULL )
	    {
	    	newRootOldRight->parent = oldRoot;
	    }
	    //update the parent of the left of root. since it was just moved to the top or where root was
	    //so the root parent, would have to be the new parent of the root left
	    newRoot->parent = oldRootParent;
	    if( oldRootParent != NULL )
	    {
	    	if( oldRootParent->key > newRoot->key )
	    	{
	    		oldRootParent->left = newRoot;	
	    	}
	    	else
	    	{
	    		oldRootParent->right = newRoot;	
	    	}
	    	printf("oldRootParent parent[%i] left[%i] right[%i]\r\n", BinaryTree_GetKey(self, oldRootParent->parent),  BinaryTree_GetKey( self, oldRootParent->left ),  BinaryTree_GetKey( self, oldRootParent->right ) );
		}

		BinaryTree_Node *newRootLeft = newRoot->left;
		BinaryTree_Node *newRootRight = newRoot->right;
	    BinaryTree_Node *newRootParent = newRoot->parent;
	    BinaryTree_Node *newRootLeftParent = NULL;
		BinaryTree_Node *newRootRightParent = NULL; 

	    if( newRootLeft != NULL )
		{
			BinaryTree_Node *newRootLeftParent = newRootLeft->parent;
		}
		if( newRootRight != NULL )
		{
			BinaryTree_Node *newRootRightParent = newRootRight->parent;
		}
		

		printf("BinaryTree_RotateRight newRoot[%i] newRootParent [%i]\r\n", BinaryTree_GetKey(self, newRoot),  BinaryTree_GetKey( self, newRootParent ) );
		printf("BinaryTree_RotateRight newRootLeft[%i]->[%i] newRootRight [%i]->[%i]\r\n", BinaryTree_GetKey(self, newRootLeft),  BinaryTree_GetKey(self, newRootLeftParent), BinaryTree_GetKey(self, newRootRight), BinaryTree_GetKey(self, newRootRightParent));
		BinaryTree_PrintTree(self, "after", root);
	}
}

static BinaryTree_RotateLeft( BinaryTree_Self *self, BinaryTree_Node *root )
{
	if( root != NULL )
	{
				/*
		struct node *x = y->left;
		struct node *T2 = x->right;

		// Perform rotation
		x->right = y;
		y->left = T2;

		*/

		printf("BinaryTree_RotateLeft %u\r\n", root->key);

		BinaryTree_PrintTree(self, "before", root);


		BinaryTree_Node *newRoot = root->right;
		BinaryTree_Node *oldRoot = root;
	    BinaryTree_Node *newRootOldLeft = newRoot->left;
	 	BinaryTree_Node *oldRootParent = root->parent;
		printf("BinaryTree_RotateLeft newRoot %i\r\n", BinaryTree_GetKey(self, newRoot));
		printf("BinaryTree_RotateLeft oldRoot %i\r\n", BinaryTree_GetKey(self, oldRoot));
		printf("BinaryTree_RotateLeft newRootOldLeft %i\r\n", BinaryTree_GetKey(self, newRootOldLeft));
		printf("BinaryTree_RotateLeft oldRootParent %i\r\n", BinaryTree_GetKey(self, oldRootParent));

		root = root->right;

	    // Perform rotation
	    //replace root left left with root
	    newRoot->left = oldRoot;
	    //update root's parent
	    oldRoot->parent = newRoot;

	    //repalce root left with whatever there was in the right side of the root left node	    
	    oldRoot->right = newRootOldLeft;
	    //if whatever was there was not null, then update the parent of said node.
	    if( newRootOldLeft != NULL )
	    {
	    	newRootOldLeft->parent = oldRoot;
	    }
	    //update the parent of the left of root. since it was just moved to the top or where root was
	    //so the root parent, would have to be the new parent of the root left
	    newRoot->parent = oldRootParent;
	     if( oldRootParent != NULL )
	    {
	    	if( oldRootParent->key > newRoot->key )
	    	{
	    		oldRootParent->left = newRoot;	
	    	}
	    	else
	    	{
	    		oldRootParent->right = newRoot;	
	    	}
	    	printf("oldRootParent parent[%i] left[%i] right[%i]\r\n", BinaryTree_GetKey(self, oldRootParent->parent),  BinaryTree_GetKey( self, oldRootParent->left ),  BinaryTree_GetKey( self, oldRootParent->right ) );
		}
		BinaryTree_Node *newRootLeft = newRoot->left;
		BinaryTree_Node *newRootRight = newRoot->right;
	    BinaryTree_Node *newRootParent = newRoot->parent;
 		BinaryTree_Node *newRootLeftParent = NULL;
		BinaryTree_Node *newRootRightParent = NULL; 

	    if( newRootLeft != NULL )
		{
			BinaryTree_Node *newRootLeftParent = newRootLeft->parent;
		}
		if( newRootRight != NULL )
		{
			BinaryTree_Node *newRootRightParent = newRootRight->parent;
		}

		printf("BinaryTree_RotateLeft newRoot[%i] newRootParent [%i]\r\n", BinaryTree_GetKey(self, newRoot),  BinaryTree_GetKey( self, newRootParent ) );
		printf("BinaryTree_RotateLeft newRootLeft[%i]->[%i] newRootRight [%i]->[%i]\r\n", BinaryTree_GetKey(self, newRootLeft),  BinaryTree_GetKey(self, newRootLeftParent), BinaryTree_GetKey(self, newRootRight), BinaryTree_GetKey(self, newRootRightParent));

	}

}

static BinaryTree_BalanceRight( BinaryTree_Self *self, BinaryTree_Node *root )
{
	if( root != NULL )
	{
		printf("BinaryTree_BalanceRight\r\n");
		BinaryTree_Node *rootLeft = root->left;
		if( rootLeft != NULL )
		{
			if( BinaryTree_GetHeight(self, rootLeft->right) > BinaryTree_GetHeight(self, rootLeft->left) )
			{
				printf("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ \r\n");
				BinaryTree_RotateLeft( self, rootLeft );
			}
		}
		BinaryTree_RotateRight( self, root );
		BinaryTree_CalculateHeight( self, root );
	}
}

static BinaryTree_BalanceLeft( BinaryTree_Self *self, BinaryTree_Node *root )
{
	if( root != NULL )
	{
		printf("BinaryTree_BalanceLeft on %u\r\n", root->key);
		BinaryTree_Node *rootRight = root->right;
		if( rootRight != NULL )
		{
			if( BinaryTree_GetHeight( self, rootRight->left ) >  BinaryTree_GetHeight( self, rootRight->right )  )
			{
				//printf("------------------------------------------------------------------------ \r\n");
				BinaryTree_RotateRight(self, rootRight);
			}
		}
		BinaryTree_RotateLeft(self, root);
		BinaryTree_CalculateHeight( self, root );
	}
}
static BinaryTree_AVLBalanceTreeHelper( BinaryTree_Self *self, BinaryTree_Node *root )
{
	if( root != NULL )
	{
		printf("BinaryTree_BalanceTreeHelper %u\r\n", root->key);
		BinaryTree_Node *rootParent = root->parent;

		//BinaryTree_CalculateHeight( self, root );

		if( BinaryTree_GetHeight( self, root->left ) > ( BinaryTree_GetHeight( self, root->right ) + 1 ) )
		{
			//balance right	
			BinaryTree_BalanceRight( self, root );
		}
		if( BinaryTree_GetHeight( self, root->right ) > ( BinaryTree_GetHeight( self, root->left ) + 1 ) )
		{
			//balance left
			BinaryTree_BalanceLeft( self, root );
		}

		BinaryTree_CalculateHeight( self, root );

		//printf("root parent [%i] \r\n", BinaryTree_GetKey( self, rootParent) );

		if( rootParent != NULL )
		{			
			BinaryTree_AVLBalanceTreeHelper( self, rootParent );
		}

		if( root->parent == NULL )
		{
			self->binaryTree = root;

		}
		//printf("BinaryTree_BalanceTreeHelper\r\n");

	}
}
BinaryTree_Node *BinaryTree_RemoveHelper(BinaryTree_Self *self, BinaryTree_Node *root, unsigned int key )
{
	if( root == NULL )
	{
		return root;
	}
	else if( key < root->key )
	{
		root->left = BinaryTree_RemoveHelper(self, root->left, key);
		if( root->left != NULL )
		{
			BinaryTree_Node *temp = root->left;
			temp->parent = root;
		}
	}
	else if( key > root->key )
	{
		root->right = BinaryTree_RemoveHelper(self, root->right, key);
		if( root->right != NULL )
		{
			BinaryTree_Node *temp = root->right;
			temp->parent = root;
		}
	}
	else
	{
		//no children
		if( root->left == NULL && root->right == NULL )
		{
			BinaryTree_FreeNode(self, root);			
			root = NULL;
		}
		else if( root->left == NULL ) 
		{
			BinaryTree_Node *temp = root;
			root = root->right;
			BinaryTree_FreeNode(self, temp);	
		}
		else if( root->right == NULL ) 
		{
			BinaryTree_Node *temp = root;
			root = root->left;
			BinaryTree_FreeNode(self, temp);			
			
		}
		else
		{
			//two children
			BinaryTree_Node *temp = BinaryTree_MinHelper( self, root->right );
			root->key = temp->key;
			root->right = BinaryTree_RemoveHelper( self, root->right, temp->key );
		}		
		BinaryTree_ReCalculateHeight( self, root );
	}
	return root;
}
//delete O(logn)
        //search for k
        //easy case, k has no children - just delete it
        //medium case, k has ONE child - one child assumes the positions of the parent, like a linked list
        //difficult case, k has two chilren - compute k's predecessor l
                                         // - then swap k and l
                                         // - then delete k (easy case)
void BinaryTree_Remove( BinaryTree_Self *self, unsigned int key )
{
	if( self->initialized == 1 )
	{
		printf("REMOVE %i\r\n", key);
		self->binaryTree = BinaryTree_RemoveHelper( self, self->binaryTree, key );		
	}
	else
	{
		printf("PLEASE USED BinaryTree_Init BEFORE TRAVERSING THROUGH FOR NODES\r\n");
	}
}

//NOT A BALANCED TREE - ORDER OF INSERTIONS MATTERS
void BinaryTree_Insert( BinaryTree_Self *self, unsigned int key )
{
	//find the node where we would want to place this new node
	//if its found, we might need to dig into the root to determine where to put the new key
	//for this implementation lets not allow repeats
	printf( "inserting %u *******************\r\n", key );
	BinaryTree_Node *root = BinaryTree_SearchHelper( self, self->binaryTree, key );
	if( root == NULL )
	{
		//null here means that the tree is empty
		self->binaryTree = BinaryTree_GetNextAvailable( self, key, NULL );
	}
	else if( root->key == key )
	{
		//key already exists ignore re-adding it.
		printf( "REPEAT %u *******************\r\n", key );
	}
	else if( root->key > key )
	{
		root->left = BinaryTree_GetNextAvailable( self, key, root );
		BinaryTree_ReCalculateHeight( self, root->left );
	}
	else
	{
		root->right = BinaryTree_GetNextAvailable( self, key, root );		
		BinaryTree_ReCalculateHeight( self, root->right );
	}
}


