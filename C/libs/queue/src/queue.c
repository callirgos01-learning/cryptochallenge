#include <stdio.h>
#include "../queue.h"
#include "queue_p.h"

static void Queue_FreeNode(Queue_Self *self, Queue_Node *toAdd)
{
    //save the address of the usedList
    Queue_Node *freeHead = self->freeList;
    if(freeHead == NULL)
    {
        self->freeList = toAdd;
        self->freeList->value = NULL;
        self->freeList->next = NULL;
    }
    else
    {
        //add the node on the top of the list
        self->freeList = toAdd;
        self->freeList->value = NULL;
        //link the new head to the next element in the usedList
        self->freeList->next = freeHead;    
    }
    
}

static Queue_Node *Queue_GetAvailableNode(Queue_Self *self, void *valueToAdd, Queue_Node *nextPointer)
{
	Queue_Node *toReturn = NULL;
	//handle case where we are out of nodes!!
	if(self->freeList != NULL)
	{
		//save the address of the next free element in the list
		Queue_Node *temp = self->freeList->next;
		//return the first element in the free list
		toReturn = self->freeList;
		//toReturn set value to incoming value
		toReturn->value = valueToAdd;
		//link to the nextinpute pointer
		toReturn->next = nextPointer;
		//save the address of the next element in the free list as the head of the free list
		self->freeList = temp;
		//return the address to the first element in the free list to be added the used list
	}
	else
	{
		printf("OUT OF NODES!!\r\n");
		//we could implement a circular linked list here, and pop out the last element in the list for this one.

	}
	return toReturn;
}

static void Queue_AddFirst( Queue_Self *self, void *valueToAdd )
{
	if( !self->initialized )
	{
		printf("UNABLE TO USE AN INITIALIZED LINKED LIST USE LinkedList_Init FIRST\r\n ");		
	}
	else
	{		
		Queue_Node *newNode = Queue_GetAvailableNode(self, valueToAdd, NULL);
		if( newNode != NULL )
		{
			//save the address of the usedList
			Queue_Node *usedHead = self->usedList;
			//put the new node on the top of the used list.		
			self->usedList = newNode;
			//link the new head to the next element in the usedList
			self->usedList->next = usedHead;


			//printf("Queue_EnQueue 0x%08X\r\n", self->usedList->value);
		}
		else
		{
			printf("operation failed, cant get more nodes\r\n");
		}
	}
}

//public interface
void Queue_Init( Queue_Self *self, Queue_Node *queueNode, unsigned int queueSize )
{
	printf("Queue_Init\r\n");
	int i;

	for( i = 0; i < queueSize - 1; i++ )
	{
		queueNode[i].value = NULL;
		queueNode[i].next = &queueNode[i+1];
	}
	queueNode[queueSize-1].value = NULL;
	queueNode[queueSize-1].next = NULL;

	self->freeList = queueNode;
	self->usedList = NULL;
	self->nodesHeapSize = queueSize; 
	self->nodeCount = 0;

	self->initialized = 1;
}

void Queue_EnQueue( Queue_Self *self, void *toQueue )
{
	Queue_AddFirst( self, toQueue );
}

void *Queue_DeQueue( Queue_Self *self )
{
	void *toReturn = 0;

	if( self->usedList != NULL )
	{
		//get last node
		Queue_Node *lastNode = self->usedList;
		Queue_Node *prev = NULL; 
		while ( (lastNode->next != NULL) )
		{
	    	prev = lastNode;
	        lastNode = lastNode->next;
		}
		
		//lastNode->next = newNode;
		Queue_Node *toFree = lastNode;          
		toReturn = toFree->value;      
	    //remove node
	    if( prev != NULL )
	    {
	    	prev->next = lastNode->next;
	    }
	    else
	    {	    	
	    	self->usedList = NULL;
	    }

	    Queue_FreeNode( self, toFree );

		//safe last node value
		//free last node
		//remove last node

		//return last node value
	}
	return toReturn;
}
char Queue_IsEmpty( Queue_Self *self )
{
	char toReturn = 1;
	if( self != NULL )
	{
		toReturn = ( self->usedList == NULL );

	}
	return toReturn;
}

