
#ifndef __QUEUE_H__ 
#define __QUEUE_H__

typedef struct
{
    void *value;
    void *next;
}Queue_Node;

typedef struct
{    
    char initialized;
    Queue_Node *freeList;
    Queue_Node *usedList;
    char nodesHeapSize;
    char nodeCount;
    void *head;
    void *tail;
}Queue_Self;


void Queue_Init( Queue_Self *self, Queue_Node *queueNode, unsigned int queueSize );
void Queue_EnQueue( Queue_Self *self, void *toQueue );
void *Queue_DeQueue( Queue_Self *self );
char Queue_IsEmpty( Queue_Self *self );
#endif // __QUEUE_H__
