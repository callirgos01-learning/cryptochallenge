
#include <stdio.h>
#include <string.h>
#include <limits.h>
#include "libs/linkedlist/linkedlist.h"
#include "libs/hashtable/hashtable.h"
#include "libs/binarytree/binarytree.h"
#include "libs/queue/queue.h"

typedef unsigned int uint;
typedef unsigned char byte;
typedef int bool;
#define true 1
#define false 0

#define NULL_TERMINATED ( 0xFFFFFFFF )
#define STRING_MAX_LENGTH ( 5000 )
#define WORD_MAX_LENGTH ( 200 )
#define DICTIONARY_LENGTH ( 5 )

static char * s_dictionary [DICTIONARY_LENGTH] = {
    "something",
    "is",
    "not",
    "right",
    "here"
};

static PrintArray( short *toPrint, int toPrintLength )
{

    int i = 0;
    printf("[ ");
    for( i = 0; i < toPrintLength; i++ )
    {
        printf("0x%04X ", toPrint[i] );
    }
    printf("]\r\n");

}
/*
static void GetStringLength( char *input, uint *inputLength )
{
    uint i;
    uint totalLength = 0;
    for( i = 0; i < STRING_MAX_LENGTH; i++ )
    {
        totalLength++;
        if( input[i] == '\0' )
        {
            break;
        }
    }
    *inputLength = totalLength;

}
*/
static bool IsAWord( char *input, uint inputOffset )
{
    int i;
    bool result = false;
    for( i = 0; i < DICTIONARY_LENGTH; i++)
    {
        result = (bool)(strcmp( input + inputOffset, s_dictionary[i] ) == 0 );
        if( result == true)
        {  
            break;
        }
    }
    return result;
}

static char *SplitString( char input[] )
{
    uint i;
    uint outputIndex = 0;
    uint startIndex = 0;
    uint inputLength = strlen(input);
    static char output[STRING_MAX_LENGTH];    
    memset( output, 0x00, STRING_MAX_LENGTH );

    for( i = 0; i < inputLength; i++ )
    {
        output[outputIndex++] = input[i];
        if( IsAWord(output, startIndex) )
        {   
            output[outputIndex++] = ' ';
            startIndex = outputIndex;
        }
    }
    return output;
}

static char *outputArray[][2]= { {"","fizz"}, {"buzz","fizzbuzz"} };

static void fizzBuzz( int input )
{
    int x = (input%3)==0;
    int y = (input%5)==0;
    printf("output[%i][%i] = %s\r\n",x,y, outputArray[x][y]);
}

static void FirstSortAttemptNoAlgo( short *input, int inputLength, 
                                    short *output, int *outputLength )
{
    //this turns out to the a version of buble sort
    uint i = 0;
    bool done = false;
    while(!done)
    {
        done = true;
        for( i = 0; i < inputLength-1; i++ )
        {
            if(input[i]>input[i+1])
            {
                short temp = input[i];
                short temp2 = input[i+1];
                input[i+1] = temp;
                input[i] = temp2;
                done = false;
            }
        }
    }

    memcpy(output, input, inputLength * sizeof(short) );
    *outputLength = inputLength;
}

static void SelectionSort( short *input, int inputLength, 
                           short *output, int *outputLength )
{
    int i = 0;
    int j = 0;
    int smallestIndex = 0;
    //selection would find the index of the smallest value in the array.
    //then we swap the smallest value from where it is, to the next index
    //in the output array, or the input array, if we want to 
    //modify the input
    //all of the indexes before j are already sorted so we dont
    //include them in the second array
    for( j = 0; j < inputLength-1; j++ )
    {
        smallestIndex = j;
        for( i = j + 1; i < inputLength; i++ )
        {
            if( input[i] < input[smallestIndex] )
            {
                smallestIndex = i;
            }
        }
        int temp = input[j];
        input[j] = input[smallestIndex];
        input[smallestIndex] = temp;
    }

    memcpy(output, input, inputLength * sizeof(short) );
    *outputLength = inputLength;
}

static void BubbleSort( short *input, int inputLength, 
                           short *output, int *outputLength )
{
    //bubble sort.
    //compare current index to current index +1.
    //swap accordingly.
    //if we dont swap in this pass, we can break
    //out of the array
    int i = 0;
    int k = 0;
    bool flag = false;
    for( k = 0; k < inputLength; k++ )
    {
        flag = false;
        for( i = 0; i < (inputLength - k - 1); i++ )
        {
            if(input[i] > input[i+1])
            {
                int temp = input[i+1];
                input[i+1] = input[i];
                input[i] = temp;            
                flag = true;
            }
        }
        if(flag == false)
        {
            break;
        }
    }

    memcpy(output, input, inputLength * sizeof(short) );
    *outputLength = inputLength;
}

static void InsertionSort( short *input, int inputLength, 
                           short *output, int *outputLength )
{
   int i = 0;
   //go through the array, 
   //select one item at a time, then shift that value
   //from where it is, to wherever it needs to be
   //by comparing this value, to the value of the previous index
   //until we get to index 0, or if the current value is not
   //smaller than the previous value.
   for( i = 0; i <inputLength; i++ )
   {
        int temp = input[i];
        int hole = i;
        while ((hole>0) && (input[hole-1]>temp) )
        {
            input[hole] = input[hole-1];
            hole = hole-1;
        }
        input[hole] = temp;
   }

    memcpy(output, input, inputLength * sizeof(short) );
    *outputLength = inputLength;
}
static void Merge( short *left, int leftLength, short *right, int rightLength, short *output)
{
    int leftIndex = 0;
    int rightIndex = 0;
    int outputIndex = 0;

    while( (leftIndex < leftLength) && (rightIndex < rightLength) ) 
    {
        if( left[leftIndex] <= right[rightIndex] )
        {
            output[outputIndex++] = left[leftIndex++];
        }
        else
        {
            output[outputIndex++] = right[rightIndex++];
        }
    }
    while( leftIndex < leftLength )
    {
        output[outputIndex++] = left[leftIndex++];
    }
    while( rightIndex < rightLength )
    {
        output[outputIndex++] = right[rightIndex++];
    }

}
static void MergeSort( short *input, int inputLength )
{    
    int n = inputLength;

    if( n < 2)
    {
        //done
        //no need to sort a single element array
    }
    else
    {
        //drawbacks, need to create a lot of extra arrays
        //sublists and whatnot
        int leftLength = n / 2;
        int rightLength = n - leftLength;
        short left[leftLength];
        short right[rightLength];
        int i = 0;
        //divide the array into two equal halves
        for(i = 0; i<leftLength; i++)
        {
            left[i] = input[i];
        }
        for(i = leftLength; i<n; i++)
        {
            right[i-leftLength] = input[i];
        }
        MergeSort( left, leftLength );
        MergeSort( right, rightLength );
        Merge(left, leftLength, right, rightLength, input);        
    }
  
}
static void MergeSortHelper( short *input, int inputLength, 
                           short *output, int *outputLength )
{
    memcpy(output, input, inputLength * sizeof(short) );
    *outputLength = inputLength;

    MergeSort( output, inputLength );
}   

static int Partition( char *input, int start, int end )
{
    int toReturn = 0;
    int pivot = input[end];
    int partIndex = start;
    int i = 0;
    for( i = start; i < end; i++ )
    {
        if( input[i] <= pivot )
        {
            //swap two values 
            //value 1 the value in pIndex, which starts in the beginning of the array
            //value 2 the value in i, 
            char temp = input[i];
            input[i] = input[partIndex];
            input[partIndex++] = temp;            
        }        
    }
    //swap pIndex with the end index so we can now have all values that are smaller than the value in pivot
    //to the left, and the values that are greater than the pivot value to the right
    char temp = input[end];
    input[end] = input[partIndex];
    input[partIndex] = temp;
    return partIndex;
}
static void QuickSort( char *input, int start, int end )
{
 
    if( start < end)
    {
        int partIndex = Partition(input, start, end);
        QuickSort(input, start, partIndex-1);
        QuickSort(input, partIndex + 1, end);
    }
}
static void QuickSortHelper( short *input, int inputLength, 
                           short *output, int *outputLength )
{
    memcpy(output, input, inputLength * sizeof(short) );
    *outputLength = inputLength;

    QuickSort( output, 0, (inputLength -1) );
}   
static void splitStringTest()
{
    printf("splitStringTest\r\n");
    char input[] = "somethingisnotrighthere\0";
    printf("input [%s]\r\n", input);
    printf("output[%s\r\n\r\n", SplitString(input));
}
static void fizzBuzzTest()
{
    printf("fizzBuzzTest\r\n");
    fizzBuzz( 20 );
    fizzBuzz( 15 );
    fizzBuzz( 25 );
    fizzBuzz( 58 );    
    printf("\r\n");    
}

typedef enum 
{
    sortingAlgos_none,
    sortingAlgos_selection,
    sortingAlgos_bubble,
    sortingAlgos_insertion,
    sortingAlgos_merge,
    sortingAlgos_quick,
}sortingAlgos;

static void sortingTests( sortingAlgos algoToUse )
{
    printf("sortingTests\r\n");
    short input[50] = { 6, 2, 12, 66, 45, 12, 34, 12, 1, 22,
                       45, 65, 76, 98, 49, 23, 3, 34, 44, 45,
                       56, 76, 88, 99, 9, 11, 22, 121, 12, 54,
                       89, 78, 44, 453, 33, 342, 656, 34, 34, 54,
                       11, 23, 45, 67, 34, 34, 23, 56, 89, 0 };
    short output[50];    
    uint outputLength = 0;
    memset( output, 0, 50 * sizeof(short));

    printf("input ");
    PrintArray( input, 50 );
    switch( algoToUse )
    {
        case sortingAlgos_none:
            printf("FirstSortAttemptNoAlgo\r\n");
            FirstSortAttemptNoAlgo( input, 50, output, &outputLength );
            break;
        case sortingAlgos_selection:
            printf("SelectionSort\r\n");
            SelectionSort( input, 50, output, &outputLength );
            break;
        case sortingAlgos_bubble:
            printf("BubbleSort\r\n");
            BubbleSort( input, 50, output, &outputLength );
            break;
        case sortingAlgos_insertion:
            printf("InsertionSort\r\n");
            InsertionSort( input, 50, output, &outputLength );
            break;
        case sortingAlgos_merge:
            printf("MergeSortHelper\r\n");
            MergeSortHelper( input, 50, output, &outputLength );
            break;
        case sortingAlgos_quick:
            printf("QuickSortHelper\r\n");
            QuickSortHelper( input, 50, output, &outputLength );
            break;
        default:
            printf("undefined");

    }    
    printf("output");
    PrintArray( output, 50 );
    printf("\r\n");    
}
#define MAX_VALUE ( 100 )
static unsigned long fib[MAX_VALUE] = { 0 , 1 };
static void  fibHelper( int input, unsigned long **result, unsigned int *resultLength )
{
    int i;   
    static int startingIndex = 2;
    if( fib[input] == 0 )
    { 
        for( i = startingIndex; i < input + 1; i++)
        {
            fib[i] = fib[i-1] + fib[i-2];
            startingIndex = i;
        }    
    }

    *result = fib;
    *resultLength = input;
}
static void FibonaciTests()
{
    unsigned long *results;
    unsigned int resultsLength = 0;

    int fibNumber = 0;    
    for( fibNumber = 0; fibNumber < 50; fibNumber++ )
    { 
        fibHelper(fibNumber, &results, &resultsLength );
        int i;
        for(i = 0; i < resultsLength; i++ )
        {
            printf("[%lu]", results[i]);    
        }
        printf("\r\n");
    }
    for( fibNumber = 50; fibNumber > 0; fibNumber-- )
    { 
        fibHelper(fibNumber, &results, &resultsLength );
        int i;
        for(i = 0; i < resultsLength; i++ )
        {
            printf("[%lu]", results[i]);    
        }
        printf("\r\n");
    }

    
}
static int binarySearch( short *input, int startingIndex, int endingIndex, int valueToSearch )
{
    if( startingIndex <= endingIndex )
    {        
        int middleIndex = startingIndex + ((endingIndex - startingIndex) / 2);

        if( valueToSearch == input[middleIndex])
        {
            return middleIndex;
        }
        else if( valueToSearch > input[middleIndex] )
        {
            //search right
            return binarySearch(input, middleIndex + 1, endingIndex, valueToSearch);
        }
        else
        {
            //search left
            return binarySearch(input, startingIndex, middleIndex-1, valueToSearch);
        }
    }
    else
    {
        return -1;
    }

}
static void binarySearchTests()
{
    printf("binarySearchTests\r\n");
    short input[8] = { 1, 2, 33, 45, 49, 67, 89, 100 };
    int indexOfOutput = 0;
    indexOfOutput = binarySearch( input, 0, 7, 1 );
    printf("output %i\r\n", indexOfOutput );
    indexOfOutput = binarySearch( input, 0, 7, 2 );
    printf("output %i\r\n", indexOfOutput );
    indexOfOutput = binarySearch( input, 0, 7, 3 );
    printf("output %i\r\n", indexOfOutput );
    indexOfOutput = binarySearch( input, 0, 7, 4 );
    printf("output %i\r\n", indexOfOutput );
    indexOfOutput = binarySearch( input, 0, 7, 5 );
    printf("output %i\r\n", indexOfOutput );
    indexOfOutput = binarySearch( input, 0, 7, 6 );
    printf("output %i\r\n", indexOfOutput );
    indexOfOutput = binarySearch( input, 0, 7, 7 );
    printf("output %i\r\n", indexOfOutput );
    indexOfOutput = binarySearch( input, 0, 7, 100 );
    printf("output %i\r\n", indexOfOutput );
    indexOfOutput = binarySearch( input, 0, 7, 45 );
    printf("output %i\r\n", indexOfOutput );
    indexOfOutput = binarySearch( input, 0, 7, 49 );
    printf("output %i\r\n", indexOfOutput );
    
}
static void ReverseArray( short *input, int inputLength )
{
    int i = 0;
    for( i = 0; i < inputLength/2; i++)
    {
        int temp = *(input+i);
        *(input+i) = *(input + (inputLength - i - 1));
        *(input + (inputLength - i - 1)) = temp;
    }

}
static void RevereseArrayTest()
{
    printf("revereseArrayTest\r\n");
    short input[50] = { 6, 2, 12, 66, 45, 12, 34, 12, 1, 22,
                       45, 65, 76, 98, 49, 23, 3, 34, 44, 45,
                       56, 76, 88, 99, 9, 11, 22, 121, 12, 54,
                       89, 78, 44, 453, 33, 342, 656, 34, 34, 54,
                       11, 23, 45, 67, 34, 34, 23, 56, 89, 0 };

    printf("input ");
    PrintArray( input, 50 );
    ReverseArray(input, 50);
    printf("output");
    PrintArray( input, 50 );
    printf("\r\n\r\n");    
}

static unsigned long pasc[MAX_VALUE][MAX_VALUE] = { 0 };
static unsigned long  pascal( int x, int y )
{
    static int pascIndexX = 0;
    static int pascIndexY = 0;
    if( pasc[x][y] == 0 )
    {
        int i = 0;
        int j = 0;
        for( i = pascIndexX; i <= x; i++ )
        {
            for( j = pascIndexY; j <= y; j++ )
            {
                if( j == 0 )
                {
                    pasc[i][j] = 1;
                }
                else if( i == 0 )
                {
                    pasc[i][j] = 1;
                }
                else
                {
                    pasc[i][j] = pasc[i-1][j] + pasc[i][j-1];
                }
            }
        }
        pascIndexX = x;
        pascIndexY = y;
    }
    return pasc[x][y];
}
static void PascalTests()
{
    int x = 0;
    int y = 0;

    for( x = 0; x < 50; x++ )
    {
        for( y = 0; y < 50; y++ )
        {
            printf("inputs[%i][%i] = %lu\r\n",x,y,pascal(x,y));
        }

    }

}
#define TESTSIZE 41
static char *valuesToTest[TESTSIZE] = { "charlie\0", "charles\0", "robert\0", "123\0", "234\0", "345\0", "546\0", "7776666\0", "SDFGADFGASDF\0", "ASDFAVADFASD\0",
                                        "ASDFASD4FTRH\0", "DFGHHJJ\0", "SDFG45\0", "SDFGXXCVB\0", "FFF\0", "SDFGSDFGSDFGSDFG\0", "FGSDFGSDFG\0", "SDFGSDFGDFGSDFGSDFG\0", "WERTWERTER\0", "SDVRRR\0", 
                                        "SEESEWSDFS\0", "DWEWEW\0", "ASSEETRTRG\0", "dead\0", "deadbeef\0", "befe\0", "beef\0", "compute\0", "yesman\0", "pmtja;lsd\0",
                                        "josdfas\0", "jaasde\0", "jo2345234523\0", "joyjhujusdfsdfe\0", "jasdfaweqwe\0", "a\0", "ab\0", "abc\0", "abcd\0", "abcde\0", "aaaaah\0" };
#define LINKEDLISTSIZE ( 5 )
static LinkedList_Self s_linkedListSelf = { 0 };
static LinkedList_Node s_linkedListNodes[LINKEDLISTSIZE] = { 0 };
static void LinkedListTests()
{
    
    //this implementation requires the client to define the block of memory to use for nodes
    //this implies the user knows the maximum number of nodes its going to need at any given time
    
    LinkedList_Init(&s_linkedListSelf, s_linkedListNodes, LINKEDLISTSIZE);

    /*
    LinkedList_AddFirst("ADDMEPL0X!!!");
    LinkedList_AddFirst("metoo!!!");
    LinkedList_AddFirst("what abot me??!!!");
    LinkedList_AddFirst("canI??");
    LinkedList_AddFirst("aww...");
    LinkedList_AddFirst("removeSOmeone!");
    */
    LinkedList_Remove(&s_linkedListSelf, "TO rEMM??");

    LinkedList_Add(&s_linkedListSelf, "ADDMEPL0X!!!");
    LinkedList_Add(&s_linkedListSelf, "metoo!!!");
    LinkedList_Add(&s_linkedListSelf, "what abot me?");
    LinkedList_Add(&s_linkedListSelf, "canI??");
    LinkedList_Add(&s_linkedListSelf, "aww...");
    LinkedList_Add(&s_linkedListSelf, "removeSOmeone!");
    
    LinkedList_Remove(&s_linkedListSelf, "what abot me?");
    LinkedList_Remove(&s_linkedListSelf, "ADDMEPL0X!!!");
    LinkedList_Remove(&s_linkedListSelf, "canI??");
    LinkedList_Remove(&s_linkedListSelf, "metoo!!!");
    LinkedList_Remove(&s_linkedListSelf, "aww...");
    LinkedList_Remove(&s_linkedListSelf, "removeSOmeone!");

    //push
    //LinkedListPush(LinkedList, 50, valuesToTest[0]);
    //pull
    //LinkedListPull(LinkedList, 50, valuesToTest[0]);
    //linear probing or separate chaining

}


static void HashTableTests( )
{
    int i;
    HashTable_Init();

    for( i = 0; i < TESTSIZE; i++ )
    {
        HashTable_InsertValue(valuesToTest[i]);
    }
    HashTable_RemoveValue("yesman");
    HashTable_RemoveValue("notInTheList");
    for( i = 0; i < TESTSIZE; i++ )
    {
        printf("[%i] %s \r\n", HashTable_IsPresent(valuesToTest[i]), valuesToTest[i] );
    }
    
}
#define TREESIZE ( 1000 )
static BinaryTree_Self s_treeSelf;
static BinaryTree_Node s_treeNodes[TREESIZE] = { 0 };
static void BinaryTreeTests()
{

    //like a sorted dynamic array 
    BinaryTree_Init(&s_treeSelf, s_treeNodes, TREESIZE);
    //insert O(log n)
    //search for key, if it terminates to a null pointer, place a new node instead of that null pointer
    //if its equal to the key then figure out at a way to handle duplicates.  
    int i;
    short input[50] = { 6, 2, 12, 66, 45, 55, 123, 124, 1, 223,
                       452, 65, 76, 98, 49, 23, 3, 34, 44, 453,
                       56, 762, 88, 99, 9, 112, 22, 121, 122, 54,
                       89, 78, 442, 454, 33, 342, 656, 343, 334, 542,
                       11, 232, 455, 67, 344, 345, 233, 562, 892, 29 };
  
    for( i = 0; i < 50; i++)
    {
        BinaryTree_Insert(&s_treeSelf, input[i]);
    } 
    BinaryTree_PrintTreeInOrder(&s_treeSelf);
    BinaryTree_PrintTreeLevelOrder(&s_treeSelf);

    //print keys in sorted order O(n)
        //recursively, print everything on the left, then then root, then everything to the right
        //where all recursive operations print ONE key
    //BinaryTree_PrintTreeInOrder(&s_treeSelf);

    
    //search O(logn)
    /*printf("binarytree[%u] %u\r\n", BinaryTree_Search(&s_treeSelf, 5), 5);
    printf("binarytree[%u] %u\r\n", BinaryTree_Search(&s_treeSelf, 49), 49);


    //select O(logn)

    //min/max O(longn)
        //find the min key
        //start at root go to the left till you find the one that has a left key as null    
    printf("min %u\r\n", BinaryTree_Min(&s_treeSelf));
        //find max key
        //start at root do the same but to the right
    printf("max %u\r\n", BinaryTree_Max(&s_treeSelf));

    //predecessor/succsesor O(logn)
        //compute the predecessor/parent of key k
        //easy case - if k's left subtree is non empty.
            //return max key in left
            //so go left once, then follow right child pointers till you cant, and that's the predecesor.
        //otherwire - follow parent pointers until you find a key smaller than your won, that is guaranteed to be your predecesor
    //key has no right tree
    printf("succ[%u] -> %u\r\n", 33, BinaryTree_Successor(&s_treeSelf, 33));
    //key has right tree
    printf("succ[%u] -> %u\r\n", 49, BinaryTree_Successor(&s_treeSelf, 49));
   
    //key is not present in the tree
    printf("succ[%u] -> %u\r\n", 5, BinaryTree_Successor(&s_treeSelf, 5));

    //no left tree available
    printf("pred[%u] <- %u\r\n", 54, BinaryTree_Predecesor(&s_treeSelf, 54));
    //left tree available
    printf("pred[%u] <- %u\r\n", 65, BinaryTree_Predecesor(&s_treeSelf, 65));
    //pred is root
    printf("pred[%u] <- %u\r\n", 1, BinaryTree_Predecesor(&s_treeSelf, 1));
    //not in tree
    printf("pred[%u] <- %u\r\n", 5, BinaryTree_Predecesor(&s_treeSelf, 5));

   */
    //**************
    //rank and select (search for the given key, and return the position) O(logn)
        //size(y) + size(z) + 1
        //population on the left, population on the right + root
        //store size at each node
        //select and rank
        //start at the root x, y is left tree, z is right tree
        //find the 17th tree ordered.
        //depends on the sub tree sizes
        // get size of tree at y, if its more than 17
        // then the 17th rank will be on the tree y.
        // if its less than 17, then we can recurse on the x subtree
        // divide and conquer selection algorithm
        // if a =  i-1 then return x
    
    //delete O(logn)
        //search for k
        //easy case, k has no children - just delete it
        //medium case, k has ONE child - one child assumes the positions of the parent, like a linked list
        //difficult case, k has two chilren - compute k's predecessor l
                                         // - then swap k and l
                                         // - then delete k (easy case)
    //remove something that's not in the tree
   
    //BinaryTree_PrintTreeInOrder(&s_treeSelf);  
    //search O(logn)
   /* for( i = 0; i < 50; i++)
    {   
        printf("binarytree[%u] %u\r\n", BinaryTree_Search(&s_treeSelf,input[i]),input[i]);
    }*/
    //search O(logn)
    //char ch;
    /*for( i = 0; i < 50; i++)
    {   
        BinaryTree_Remove(&s_treeSelf, input[i] );   
        //puts("Press any key to continue...");
        //ch = getchar();
    }*/

    //search O(logn)
   /* for( i = 0; i < 50; i++)
    {   
        printf("binarytree[%u] %u\r\n", BinaryTree_Search(&s_treeSelf,input[i]),input[i]);
    }*/
    
    //BinaryTree_PrintTreeInOrder(&s_treeSelf);
   
}
#define QUEUESIZE (5000)
static Queue_Self queueSelf;
static Queue_Node queueNodes[QUEUESIZE];

static void QueueTests()
{
    Queue_Init( &queueSelf, queueNodes, QUEUESIZE );
    printf( "Queue_IsEmpty %u\r\n", Queue_IsEmpty( &queueSelf ) );

    Queue_EnQueue(&queueSelf,(void*)0xAAAAAAAA);
    Queue_EnQueue(&queueSelf,(void*)0xBBBBBBBB);
    Queue_EnQueue(&queueSelf,(void*)0xCCCCCCCC);
    Queue_EnQueue(&queueSelf,(void*)0xDDDDDDDD);
    Queue_EnQueue(&queueSelf,(void*)0xEEEEEEEE);
    Queue_EnQueue(&queueSelf,(void*)0xFFFFFFFF);
    Queue_EnQueue(&queueSelf,(void*)0x12121212);
    Queue_EnQueue(&queueSelf,(void*)0x11111111);
    Queue_EnQueue(&queueSelf,(void*)0x22222222);

    printf( "Queue_IsEmpty %u\r\n", Queue_IsEmpty( &queueSelf ) );

    printf( "       Queue_DeQueue 0x%08X\r\n", Queue_DeQueue(&queueSelf) );
    printf( "       Queue_DeQueue 0x%08X\r\n", Queue_DeQueue(&queueSelf) );
    printf( "       Queue_DeQueue 0x%08X\r\n", Queue_DeQueue(&queueSelf) );
    printf( "       Queue_DeQueue 0x%08X\r\n", Queue_DeQueue(&queueSelf) );
    printf( "       Queue_DeQueue 0x%08X\r\n", Queue_DeQueue(&queueSelf) );
    printf( "       Queue_DeQueue 0x%08X\r\n", Queue_DeQueue(&queueSelf) );    
    printf( "       Queue_DeQueue 0x%08X\r\n", Queue_DeQueue(&queueSelf) );
    printf( "       Queue_DeQueue 0x%08X\r\n", Queue_DeQueue(&queueSelf) );
    printf( "       Queue_DeQueue 0x%08X\r\n", Queue_DeQueue(&queueSelf) );

    printf( "Queue_IsEmpty %u\r\n", Queue_IsEmpty( &queueSelf ) );
}

#define GRAPH_WIDTH  ( 20 )
#define GRAPH_HEIGHT ( 20 )
typedef struct
{
    unsigned int x;
    unsigned int y;
    unsigned int value;
    char explored;
    unsigned int layer;    
}Graph_Node;

typedef struct 
{    
    unsigned int count;
    Queue_Self queueSelf;
    Queue_Node queueNodes[QUEUESIZE];    
    Graph_Node graph[GRAPH_WIDTH][GRAPH_HEIGHT];
    unsigned int width;
    unsigned int height;
}Graph_Self;
static Graph_Self graph = { 0 };

static void Graph_Init( Graph_Self *self, unsigned int width, unsigned int height )
{    
    int h;
    int w;

    Queue_Init( &self->queueSelf, self->queueNodes, QUEUESIZE );
    self->width = width;
    self->height = height;
    self->count = 1;

    for( h = 0; h < height; h++ )
    {   
        for( w = 0; w < width; w++ )
        {
            //printf("[%u][%u]\r\n", h, w );
            self->graph[w][h].layer = 0;
            self->graph[w][h].value = 0;
            self->graph[w][h].explored = 0;
            self->graph[w][h].y = h;
            self->graph[w][h].x = w;
        }
    }

}

static Graph_Node *Graph_GetNode( Graph_Self *self, unsigned int x, unsigned y )
{
    Graph_Node *toReturn = NULL;
    if( ( x < self->width ) && ( y < self->height ) )
    {
        toReturn = &self->graph[x][y];
    }
    return toReturn;

}
static void Graph_BFSPrintIterative( Graph_Self *self, unsigned int x, unsigned y )
{
    int count = 1;
    printf("Graph_BFSPrintIterative\r\n");

    Graph_Node *rightChild = Graph_GetNode( self, x + 1, y );
    Graph_Node *bottomChild = Graph_GetNode( self, x, y + 1 );
    
    if( rightChild != NULL )
    {
        Queue_EnQueue( &self->queueSelf, rightChild );
    }
    if( bottomChild != NULL )
    {
        Queue_EnQueue( &self->queueSelf, bottomChild );
    }
    
    self->graph[x][y].value = count++;
    self->graph[x][y].explored = 1;

    printf( "(%u,%u)%04u\r\n", self->graph[x][y].y, self->graph[x][y].x, self->graph[x][y].value );
    while( Queue_IsEmpty( &self->queueSelf ) == 0 )
    {        
        rightChild = NULL;
        bottomChild = NULL;

        Graph_Node *processNode = Queue_DeQueue(&self->queueSelf);
        if( processNode->explored == 0 )
        {   
            rightChild = Graph_GetNode( self,  processNode->x + 1, processNode->y );
            bottomChild = Graph_GetNode( self, processNode->x,     processNode->y + 1 );
            
            if( rightChild != NULL )
            {
                Queue_EnQueue( &self->queueSelf, rightChild );
            }
            if( bottomChild != NULL )
            {
                Queue_EnQueue( &self->queueSelf, bottomChild );
            }
            processNode->value = count++;
            processNode->explored = 1;
            printf("(%u,%u)%04u\r\n", processNode->y, processNode->x, processNode->value );
        }
    }


}

static void Graph_BFSPrintRecursive( Graph_Self *self, Graph_Node *startNode )
{   
    if( (self != NULL) && (startNode != NULL) )
    {
        if( startNode->explored == 0 )
        {
            Graph_Node *rightChild = Graph_GetNode( self,  startNode->x + 1, startNode->y );
            Graph_Node *bottomChild = Graph_GetNode( self, startNode->x,    startNode->y + 1 );

            if( rightChild != NULL )
            {
                Queue_EnQueue( &self->queueSelf, rightChild );
            }
            if( bottomChild != NULL )
            {
                Queue_EnQueue( &self->queueSelf, bottomChild );
            }
            
            startNode->value = self->count++;
            startNode->explored = 1;
        }
        Graph_BFSPrintRecursive( self, Queue_DeQueue(&self->queueSelf) );
    }
}


static void Graph_Print( Graph_Self *self )
{
    int i, j;
    for( i = 0; i < self->height; i++ )
    {   
        for( j = 0; j < self->width; j++ )
        {
            printf("[%02u][%u](%02u,%02u)%04u ", self->graph[j][i].layer, self->graph[j][i].explored, self->graph[j][i].y, self->graph[j][i].x, self->graph[j][i].value);
        }     
        printf("\r\n");       
        
    }
    printf("\r\n");    

}
static void Graph_ResetExplored( Graph_Self *self )
{
    int i, j;
    for( i = 0; i < self->height; i++ )
    {   
        for( j = 0; j < self->width; j++ )
        {
           self->graph[j][i].explored = 0;
           self->graph[j][i].layer = 0;
        }
    }

}


static void Graph_BFSShortestPathRecursive( Graph_Self *self, Graph_Node *startNode, Graph_Node *endNode, int *output )
{
    if( (self != NULL) && (startNode != NULL) )
    {
        Graph_Node *rightChild  = Graph_GetNode( self, startNode->x + 1, startNode->y );
        Graph_Node *bottomChild = Graph_GetNode( self, startNode->x,     startNode->y + 1 );
        Graph_Node *leftChild   = Graph_GetNode( self, startNode->x - 1, startNode->y );
        Graph_Node *aboveChild  = Graph_GetNode( self, startNode->x,     startNode->y - 1 );

        startNode->explored = 1;

        if( ( aboveChild != NULL ) && ( aboveChild->explored == 0 ))
        {                    
            aboveChild->explored = 1;
            aboveChild->layer = startNode->layer + 1;
            Queue_EnQueue( &self->queueSelf, aboveChild );
        }
        if( ( rightChild != NULL ) && ( rightChild->explored == 0 ))
        {
            rightChild->explored = 1;
            rightChild->layer = startNode->layer + 1;
            Queue_EnQueue( &self->queueSelf, rightChild );
        }
        if( ( bottomChild != NULL ) && ( bottomChild->explored == 0 ))
        {
            bottomChild->explored = 1;
            bottomChild->layer = startNode->layer + 1;
            Queue_EnQueue( &self->queueSelf, bottomChild );
        }
        if( ( leftChild != NULL ) && ( leftChild->explored == 0 ))
        {
            leftChild->explored = 1;
            leftChild->layer = startNode->layer + 1;
            Queue_EnQueue( &self->queueSelf, leftChild );
        }

        printf("([%u]%u==%u)", startNode->layer, startNode->value, endNode->value ) ;

        if( ( startNode->x == endNode->x ) && ( startNode->y == endNode->y ) )
        {
            *output = startNode->layer;
        }

    }
    if( *output == INT_MAX )
    {
        Graph_BFSShortestPathRecursive( self, Queue_DeQueue(&self->queueSelf), endNode, output );    
    }
    else
    {
        //we are dont traversing.
    }

}
static void GraphTests()
{
    Graph_Init( &graph, GRAPH_WIDTH, GRAPH_HEIGHT );
    Graph_ResetExplored( &graph );
    Graph_Print( &graph );
    Graph_ResetExplored( &graph );
    Graph_BFSPrintRecursive( &graph, &graph.graph[0][0] );
    Graph_ResetExplored( &graph );
    Graph_BFSPrintIterative  ( &graph, 0, 0 );
    Graph_ResetExplored( &graph );
    Graph_Print( &graph );
    int dst = INT_MAX;    
    Graph_BFSShortestPathRecursive( &graph, &graph.graph[1][1], &graph.graph[3][10], &dst );
    printf("dst = %i\r\n", dst );
    Graph_Print( &graph );

    /*
    Graph_ResetExplored( &graph );
    Graph_BFSShortestPathIterative( &graph, &graph.graph[0][0], &graph.graph[19][19], &dst );
    printf("dst = %i\r\n", dst );
    Graph_Print( &graph );
    */


}
static char UniqueCharacters(char *inputString)
{
    char toReturn = 1;
    int i;
    char count[256] = { 0 };
    for( i = 0; i < strlen(inputString); i++ )
    {
        printf("([%u] %u)",inputString[i],count[inputString[i]]);struct $
        {
            /* data */
        };
        if( (inputString[i] != ' ') && ((count[inputString[i]]) > 0) )
        {   
            toReturn = 0;
            break;
        }
        count[inputString[i]]++;
    }

    printf("\r\n");
    return toReturn;
}

static void c1q1()
{
    /*
    Implement an algorithm to determine if a string has all uniquq characters.
    What if you can not use additional data structures
    */
    char input[] = {"Does This String Have Repeat Characters??\0"};
    char result = UniqueCharacters(input); 

    char input2[] = {"No Rep Chrs k?\0"};
    char result2 = UniqueCharacters(input2); 

    printf("%s [%u]\r\n",input, result);
    printf("%s [%u]\r\n",input2, result2);

}
static void swap( char *source, int startIndex, int endIndex )
{
    if( source != NULL )
    {
        char temp = source[startIndex];
        source[startIndex] = source[endIndex];
        source[endIndex] = temp;
    }
}
static void ReverseString(char *inputString)
{
    if( inputString != NULL )
    {
        unsigned int stringLength = strlen(inputString);
        int i;
        for( i = 0; i < stringLength / 2; i++ )
        {
            swap(inputString, i, (stringLength - i) - 1);
        }
    }    
}
static void c1q2()
{
    /*
    Write Code to reverse a C-Style string.
    C-String means abcd is represented as 5 charactersit including null character
    */

    char input[] = {"Does This String Have Repeat Characters?\0"};
    printf("%s -> ",input);
    ReverseString(input); 
    printf("%s\r\n",input);

    char input2[] = {"No Rep Chrs k?\0"};
    printf("%s -> ",input2);
    ReverseString(input2); 
    printf("%s\r\n",input2);

}

static void removeAtIndex( char *source, int sourceLength, int indexToRemove )
{
    int i;
    for( i = indexToRemove; i < sourceLength-1; i++)
    {
        source[i] = source[i+1];
    }
    source[sourceLength-1] = '\0';
}

static void RemoveRepeated( char* inputString )
{
    int i;
    char count[256] = { 0 };
    for( i = 0; i < strlen(inputString); i++ )
    {
        //printf("([%02X] %u %c)\r\n",inputString[i],count[inputString[i]],inputString[i]);
        if( (inputString[i] != ' ') && ((count[inputString[i]]) > 0) )
        {   
            removeAtIndex(inputString, strlen(inputString), i);
            i--;
        }
        count[inputString[i]]++;
    }

    //printf("\r\n");
}

static void c1q3()
{
    /*
    Design an algorithm and write code to remove the duplicate characters in a string without using any additional buffer.
    NOTE: one or two additional variables are fine
    And extra copy of the array is not
    FOLLOW UP
    Write the test cases for this method
    */
    char input[] = {"Does This String Have Repeat Characters?\0"};
    char expectedOutput[] = {"Does Thi Strng Hav Rp Cc?\0"};
    printf( "%s -> ",input );
    RemoveRepeated( input ); 
    printf( "%s\r\n", input );
    printf( "Expected output %s \r\n", expectedOutput );
    printf( "testing %u \r\n", ( strcmp(input, expectedOutput) == 0 ) );
}

static char IsAnagram( char* string1, int string1Length, char* string2, int string2Length )
{
    char result = 1;
    if( string1Length != string2Length )
    {
        result = 0;
    }
    if( result == 1 )
    {
        int i;
        QuickSort( string1, 0, string1Length -1 );
        QuickSort( string2, 0, string2Length -1 );
        for( i = 0; i < string1Length; i++ )
        {

            if( string1[i] != string2[i] )
            {
                result = 0;
                break;
            }
        }
    }
    return result;
}
static void c1q4()
{
    /*
    Write a method to decide if two strings are anagrams or not
    */
    char string1[28] = { "usingthesamenumberofletters\0" };
    char string2[28] = { "lettersusingnumberofthesame\0" };

    char isAnagram = IsAnagram( string1, 28, string2, 28 );

    printf( "isAnagram %u \r\n", isAnagram );


    char string3[28] = { "usithesamenumrofletters\0" };
    char string4[28] = { "lettersusinumberoftheme\0" };

    isAnagram = IsAnagram( string3, 28, string4, 28 );
    
    printf( "isAnagram %u \r\n", isAnagram );

}

static void ReplaceSpaces(char *inputString)
{
    int inputStringLength = strlen(inputString);
    int i;
    for( i=0; i<inputStringLength; i++ )
    {
        if( *(inputString+i) == ' ' )
        {
            *(inputString+i) = '\0x20';   
        }
    }

}

static void c1q5() 
{
    /*
    Write a method to replace all spaces in a string with ‘%20’
    */
    char string1[28] = { "u singthes amenumbe rofle tters\0" };
    char string2[28] = { "le ttersus ingnumb erofthes ame\0" };

    ReplaceSpaces( string1 );
    ReplaceSpaces( string2 );

    printf(" output %s\r\n", string1);
    printf(" output %s\r\n", string2);
}
#define N ( 10 )
static void c1q6()
{
    /*
    Given an image represented by an NxN matrix, where each pixel in the image is 4 bytes, write a method to rotate the image by 90 degrees Can you do this in place?
    */


    unsigned int image[N][N] = { 25 };
    int x;
    int y;
    int count = 'A';
        //initialize matrix
    for( x = 0; x < N; x++ )
    {
        for( y = 0; y < N; y++ )   
        {
            image[x][y] = count;
        }
        count++;
    }
    //print initial matrix
    for( x = 0; x < N; x++ )
    {
        for( y = 0; y < N; y++ )   
        {
            printf("[%C]",image[x][y]);
        }
        printf("\r\n");
    }
    printf("\r\n");
    //rotate 90 deegrees
    int halfN = N /2;
    for( x = 0; x < halfN; x++ )
    {
        for( y = 0; y < halfN; y++ )   
        {
            
            int endX = (N -1) - x;
            int endY = (N -1) - y;
                     
            //rotate left
            unsigned int temp = image[x][y];
            image[x][y] = image[y][endX];
            image[y][endX] = image[endX][endY];            
            image[endX][endY] = image[endY][x];
            image[endY][x] = temp;
            
        }
    }    


    //print initial matrix
    for( x = 0; x < N; x++ )
    {
        for( y = 0; y < N; y++ )   
        {
            printf("[%C]",image[x][y]);
        }
        printf("\r\n");
    } 
    printf("\r\n");

    for( x = 0; x < halfN; x++ )
    {
        for( y = 0; y < halfN; y++ )   
        {
            
            int endX = (N -1) - x;
            int endY = (N -1) - y;
            //rotate right
            unsigned int temp = image[x][y];
            image[x][y] = image[endY][x];
            image[endY][x] = image[endX][endY];            
            image[endX][endY] = image[y][endX];
            image[y][endX] = temp;           
           
            
        }
    }    
    //print initial matrix
    for( x = 0; x < N; x++ )
    {
        for( y = 0; y < N; y++ )   
        {
            printf("[%C]",image[x][y]);
        }
        printf("\r\n");
    }
    
    printf("\r\n");

}

#define M ( 5 )

static void SetToZero( unsigned int inputMatrix[M][N], int x, int y )
{
    int i;
    int u;
    for( i=0; i<N; i++ )
    {
        inputMatrix[x][i] = 0;
    }
        
    for( u=0; u<M; u++ )
    {
        inputMatrix[u][y] = 0;
    }

}
static void c1q7()
{
    printf("Chapter 1 Question 7\r\n");
    /*
    Write and algorithm such that if an element is an MxN matrix is 0, the entire row and column is set to 0
    */
    unsigned int testMatrix[M][N] = { 0 };
    int x;
    int y;
    int count = 'A';
    for( x=0; x<M; x++ )
    {
        for( y=0; y<N; y++ )
        {
            testMatrix[x][y] = count;
        }
        count++;
    }

    testMatrix[2][5] = 0;

    //print  matrix
    for( x = 0; x < M; x++ )
    {
        for( y = 0; y < N; y++ )   
        {
            printf("[%C]", testMatrix[x][y]);
        }
        printf("\r\n");
    }     
    printf("\r\n");
    char breakOUt = 0;
    for( x=0; x<M; x++ )
    {
        for( y=0; y<N; y++ )
        {
            if( testMatrix[x][y] == 0 )
            {
                SetToZero( testMatrix, x, y );
                breakOUt = 1;
                break;
            }
        }
        if( breakOUt == 1 )
        {
            break;
        }
    }

    //print  matrix
    for( x = 0; x < M; x++ )
    {
        for( y = 0; y < N; y++ )   
        {
            printf("[%C]", testMatrix[x][y]);
        }
        printf("\r\n");
    }     
    printf("\r\n");

}

static void Part1()
{
    c1q1();
    c1q2();
    c1q3();
    c1q4();
    c1q5();
    c1q6();
    c1q7();
}
//static int numberOfCombinations  = 0;

static int  possibleAddition( unsigned int *n, int nLength, int target, int runningSum, int *numberOfCombinations ) 
{        
    printf("runningSum %u\r\n ", runningSum);
    printf("*n %u\r\n ", *n);
    
    if( target == runningSum )
    {
        *numberOfCombinations += 1;
        printf("numberOfCombinations %u\r\n ", *numberOfCombinations);
    }
    if( runningSum > target )
    {
        //do nothing
    }
    else
    {
        int i;
        for( i = 0; i < nLength; i++ )
        {            
            possibleAddition( n+i, nLength-i, target, runningSum + *(n), numberOfCombinations );
        }
    }
    return *numberOfCombinations;
} 


int main( void )
{
    
    splitStringTest();    
    fizzBuzzTest();   
    sortingTests(sortingAlgos_none);
    sortingTests(sortingAlgos_selection);
    sortingTests(sortingAlgos_bubble);
    sortingTests(sortingAlgos_insertion);
    sortingTests(sortingAlgos_merge);
    sortingTests(sortingAlgos_quick);    
    FibonaciTests();
    RevereseArrayTest();
    PascalTests();    
    LinkedListTests();    
    HashTableTests();
    BinaryTreeTests();
    QueueTests();
    GraphTests();
    //split string using some search algo
    
    
    return 0;
}

